# frozen_string_literal: true
require "digest"

class Config
  downcase = ->(v) { v.downcase }
  hashed = ->(v) { Digest::SHA256.hexdigest(v.downcase) }
  hashed_64 = ->(v) { Digest::SHA256.base64digest(v.downcase) }

  PLAIN = {
    symbol: :plain,
    label: "Plain set",
    file: "config/weak_passwords_plain.yml",
    normalizer: downcase
  }

  HASHED = {
    symbol: :hashed,
    label: "Hashed set (hex)",
    file: "config/weak_passwords_hashed.yml",
    normalizer: hashed
  }

  HASHED_64 = {
    symbol: :hashed_64,
    label: "Hashed set (base 64)",
    file: "config/weak_passwords_hashed_64.yml",
    normalizer: hashed_64
  }

  BLOOMER_1000 = {
    symbol: :b_1000,
    label: "Bloomer, 1 in 1000 errors",
    file: "config/weak_passwords_bloomer_1000.bin",
    normalizer: hashed,
    false_positive: 0.001
  }

  BLOOMER_1000_HASHED = {
    symbol: :b_1000_hashed,
    label: "Bloomer hashed, 1 in 1000 errors",
    file: "config/weak_passwords_bloomer_1000_hashed.bin",
    normalizer: hashed,
    false_positive: 0.001
  }

  BLOOMER_10K = {
    symbol: :b_10K,
    label: "Bloomer, 1 in 10K errors",
    file: "config/weak_passwords_bloomer_10K.bin",
    normalizer: downcase,
    false_positive: 0.0001
  }

  BLOOM_FILTER_1000 = {
    symbol: :bf_1000,
    label: "bloom-filter, 1 in 1000 errors",
    file: "config/weak_passwords_bloom_filter_1000.bin",
    normalizer: downcase,
    false_positive: 0.001
  }

  BLOOM_FILTER_1000_HASHED = {
    symbol: :bf_1000_hashed,
    label: "bloom-filter hashed, 1 in 1000 errors",
    file: "config/weak_passwords_bloom_filter_1000_hashed.bin",
    normalizer: hashed,
    false_positive: 0.001
  }

  BLOOM_FILTER_10K = {
    symbol: :bf_10K,
    label: "bloom-filter, 1 in 10k errors",
    file: "config/weak_passwords_bloom_filter_10K.bin",
    normalizer: downcase,
    false_positive: 0.0001
  }

  CONFIGS = [
    PLAIN,
    HASHED,
    HASHED_64,
    BLOOMER_1000,
    BLOOMER_1000_HASHED,
    BLOOMER_10K,
    BLOOM_FILTER_1000,
    BLOOM_FILTER_1000_HASHED,
    BLOOM_FILTER_10K
  ]

  MAX_LENGTH = CONFIGS.map { |x| x[:label].length }.max
end
