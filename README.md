# Benchmark weak password lists

This is a benchmark for weak password lists.

We compare:

- Ruby Set of plain passwords
- Ruby Set of password hashes (hex)
- Ruby Set of password hashes (base64)
- Bloomfilter with [bloomer](https://rubygems.org/gems/bloomer) (Pure Ruby)
- Bloomfilter with [bloom-filter](https://rubygems.org/gems/bloom-filter) (C extension)

## Running locally

```bash
bundle install
bin/update-weak-passwords
bin/benchmark
```

## Results

Data: https://raw.githubusercontent.com/danielmiessler/SecLists/master/Passwords/Common-Credentials/10-million-password-list-top-1000000.txt

Checking passwords that are _on the list_

```
Checking 474743 passwords from the list
                                            user     system      total        real
Plain set                               0.099501   0.001868   0.101369 (  0.101402)
Hashed set (hex)                        0.452233   0.129596   0.581829 (  0.582780)
Hashed set (base 64)                    0.584794   0.020768   0.605562 (  0.605631)
Bloomer, 1 in 1000 errors               1.893945   0.033168   1.927113 (  1.928857)
Bloomer hashed, 1 in 1000 errors        1.879541   0.026130   1.905671 (  1.905961)
Bloomer, 1 in 10K errors                1.729052   0.017658   1.746710 (  1.746831)
bloom-filter, 1 in 1000 errors          0.075786   0.000541   0.076327 (  0.076339)
bloom-filter hashed, 1 in 1000 errors   0.457090   0.010525   0.467615 (  0.467699)
bloom-filter, 1 in 10k errors           0.074295   0.000233   0.074528 (  0.074528)
```

Checking passwords that are **not** _on the list_:

```
Checking 500000 passwords which are not on the list against it
                                            user     system      total        real
Plain set                               0.196658   0.005354   0.202012 (  0.202084)
Hashed set (hex)                        0.571929   0.013484   0.585413 (  0.585476)
Hashed set (base 64)                    0.676324   0.012740   0.689064 (  0.689300)
Bloomer, 1 in 1000 errors               1.767378   0.029708   1.797086 (  1.798174)
Bloomer hashed, 1 in 1000 errors        1.751041   0.030124   1.781165 (  1.782209)
Bloomer, 1 in 10K errors                1.395388   0.018949   1.414337 (  1.415797)
bloom-filter, 1 in 1000 errors          0.159405   0.000824   0.160229 (  0.160324)
bloom-filter hashed, 1 in 1000 errors   0.558803   0.011356   0.570159 (  0.570167)
bloom-filter, 1 in 10k errors           0.158157   0.002681   0.160838 (  0.161029)
False positives:
---
Bloomer, 1 in 1000 errors: 487
Bloomer hashed, 1 in 1000 errors: 487
Bloomer, 1 in 10K errors: 51
bloom-filter, 1 in 1000 errors: 465
bloom-filter hashed, 1 in 1000 errors: 561
bloom-filter, 1 in 10k errors: 48
```

File sizes:

```
-rw-r--r--  1 tanuki  staff   5.5M Sep 13 00:14 config/weak_passwords_plain.yml
-rw-r--r--  1 tanuki  staff    30M Sep 13 00:14 config/weak_passwords_hashed.yml
-rw-r--r--  1 tanuki  staff    21M Sep 13 00:14 config/weak_passwords_hashed_64.yml
-rw-r--r--  1 tanuki  staff   833K Sep 13 00:14 config/weak_passwords_bloomer_1000.bin
-rw-r--r--  1 tanuki  staff   833K Sep 13 00:14 config/weak_passwords_bloomer_1000_hashed.bin
-rw-r--r--  1 tanuki  staff   1.1M Sep 13 00:14 config/weak_passwords_bloomer_10K.bin
-rw-------  1 tanuki  staff   833K Sep 13 00:14 config/weak_passwords_bloom_filter_1000.bin
-rw-------  1 tanuki  staff   833K Sep 13 00:14 config/weak_passwords_bloom_filter_1000_hashed.bin
-rw-------  1 tanuki  staff   1.1M Sep 13 00:14 config/weak_passwords_bloom_filter_10K.bin
```
